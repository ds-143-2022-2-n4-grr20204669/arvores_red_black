#include <stdio.h>
#include <stdlib.h>

#define Red 1
#define Black 0

struct No {
    int info;
    struct No *esq;
    struct No *dir;
    int cor;
};

arvores_red_black* cria_arv_red_black() {
    arvores_red_black* raiz = (arvores_red_black*) malloc(sizeof(arvores_red_black));
    if(raiz != NULL) {
        *raiz = NULL;
    }
    return raiz;
}

void libera_No(struct No* no) {
    if(no == NULL)
        return;

    libera_No(no->esq);
    libera_No(no->dir);
    free(no);
    no = NULL;
}

void libera_arv_red_black(arvores_red_black* raiz) {
    if(raiz == NULL)
        return;

    libera_No(*raiz);
    free(raiz);
}

int consulta_arv_red_black(arvores_red_black *raiz, int valor) {
    if(raiz == NULL)
        return 0;

    struct No* atual = *raiz;

    while(atual != NULL) {
        if(valor == atual->info) {
            return 1;
        }

        if(valor > atual->info)
            atual = atual->dir;
        else
            atual = atual->esq;
    }
    return 0;
}

struct No* rotacionaEsquerda(struct No* A) {
    struct No* B = A->dir;

    A->dir = B->esq;
    B->esq = A;
    B->cor = A->cor;
    A->cor = Red;

    return B;
}

struct No* rotacionaDireita(struct No* A) {
    struct No* B = A->esq;

    A->esq = B->dir;
    B->dir = A;
    B->cor = A->cor;
    A->cor = Red;

    return B;
}

int cor(struct No* H) {
    if(H == NULL)
        return Black;
    else
        return H->cor;
}

void trocaCor(struct No* H) {
    H->cor = !H->cor;

    if(H->esq != NULL)
        H->esq->cor = !H->esq->cor;

    if(H->dir != NULL)
        H->dir->cor = !H->dir->cor;
}

struct No* insereNo(struct No* H, int valor, int *resp) {
    if(H == NULL) {
        struct No *novo;
        novo = (struct No*)malloc(sizeof(struct No));
        if(novo == NULL) {
            *resp = 0;
            return NULL;
        }

        novo->info = valor;
        novo->cor = Red;
        novo->dir = NULL;
        novo->esq = NULL;
        *resp = 1;
        return novo;
    }

    if(valor == H->info)
        *resp = 0;
    else{
        if(valor < H->info)
            H->esq = insereNo(H->esq, valor, resp);
        else
            H->dir = insereNo(H->dir, valor, resp);
    }

    if(cor(H->dir) == Red && cor(H->esq) == Black)
        H = rotacionaEsquerda(H);

    if(cor(H->esq) == Red && cor(H->esq->esq) == Red)
        H = rotacionaDireita(H);

    if(cor(H->esq) == Red && cor(H->dir) == Red)
        trocaCor(H);

    return H;
}

int insere_arv_red_black(arvores_red_black* raiz, int valor) {
    int resp;

    *raiz = insereNO(*raiz, valor, &resp);

    if((*raiz) != NULL)
        (*raiz)->cor = Black;

    return resp;
}

struct No* balancear(struct No* H) {
    if(cor(H->dir) == Red)
        H = rotacionaEsquerda(H);

    if(H->esq != NULL && cor(H->esq) == Red && cor(H->esq->esq) == Red)
        H = rotacionaDireita(H);

    if(cor(H->esq) == Red && cor(H->dir) == Red)
        trocaCor(H);

    return H;
}

struct No* move_2_Esq_Red(struct No* H) {
    trocaCor(H);

    if(cor(H->dir->esq) == Red) {
        H->dir = rotacionaDireita(H->dir);
        H = rotacionaEsquerda(H);
        trocaCor(H);
    }
    return H;
}

struct No* move_2_Dir_Red(struct No* H) {
    trocaCor(H);

    if(cor(H->esq->esq) == Red) {
        H = rotacionaDireita(H);
        trocaCor(H);
    }
    return H;
}

struct No* removerMenor(struct No* H) {
    if(H->esq == NULL) {
        free(H);
        return NULL;
    }

    if(cor(H->esq) == Black && cor(H->esq->esq) == Black)
        H = move_2_Esq_Red(H);

    H->esq = removerMenor(H->esq);

    return balancear(H);
}

struct No* procuraMenor(struct No* atual) {
    struct No *no1 = atual;
    struct No *no2 = atual->esq;

    while(no2 != NULL) {
        no1 = no2;
        no2 = no2->esq;
    }
    return no1;
}

struct No* remove_No(struct No* H, int valor) {
    if(valor < H->info) {
        if(cor(H->esq) == Black && cor(H->esq->esq) == Black)
            H = move_2_Esq_Red(H);

        H->esq = remove_No(H->esq, valor);
    } else {
        if(cor(H->esq) == Red)
            H = rotacionaDireita(H);

        if(valor == H->info && (H->dir == NULL)) {
            free(H);
            return NULL;
        }

        if(cor(H->dir) == Black && cor(H->dir->esq) == Black)
            H = move_2_Dir_Red(H);

        if(valor == H->info) {
            struct No* x = procuraMenor(H->dir);
            H->info = x->info;
            H->dir = removerMenor(H->dir);
        } else
            H->dir = remove_No(H->dir, valor);
    }
    return balancear(H);
}

int remove_arv_red_black(arvores_red_black *raiz, int valor) {
    if(consulta_arv_red_black(raiz, valor)) {
        struct No* h = *raiz;
        *raiz = remove_No(h, valor);

        if(*raiz != NULL)
            (*raiz)->cor = Black;

        return 1;
    } else
        return 0;
}

int estaVazia_arv_red_black(arvores_red_black *raiz) {
    if(raiz == NULL)
        return 1;

    if(*raiz == NULL)
        return 1;

    return 0;
}

int totalNo_arv_red_black(arvores_red_black *raiz) {
    if (raiz == NULL)
        return 0;

    if (*raiz == NULL)
        return 0;

    int alt_esq = totalNo_arv_red_black(&((*raiz)->esq));
    int alt_dir = totalNo_arv_red_black(&((*raiz)->dir));

    return (alt_esq + alt_dir + 1);
}

int altura_arv_red_black(arvores_red_black *raiz) {
    if (raiz == NULL)
        return 0;

    if (*raiz == NULL)
        return 0;

    int alt_esq = altura_arv_red_black(&((*raiz)->esq));
    int alt_dir = altura_arv_red_black(&((*raiz)->dir));

    if (alt_esq > alt_dir)
        return (alt_esq + 1);
    else
        return(alt_dir + 1);
}

void  posOrdem_arv_red_black(arvores_red_black *raiz, int H) {
    if(raiz == NULL)
        return;

    if(*raiz != NULL) {
        posOrdem_arv_red_black(&((*raiz)->esq), H + 1);
        posOrdem_arv_red_black(&((*raiz)->dir), H + 1);

        if((*raiz)->cor == Red)
            printf("%d  Vermelho = H(%d) \n", (*raiz)->info, H);
        else
            printf("%d  Preto = H(%d) \n", (*raiz)->info, H);

    }
}

void emOrdem_arv_red_black(arvores_red_black *raiz, int H) {
    if(raiz == NULL)
        return;

    if(*raiz != NULL) {
        emOrdem_arv_red_black(&((*raiz)->esq), H + 1);

        if((*raiz)->cor == Red)
            printf("%dR = H(%d) \n", (*raiz)->info, H);
        else
            printf("%dB = H(%d) \n", (*raiz)->info, H);

        emOrdem_arv_red_black(&((*raiz)->dir), H + 1);
    }
}

void preOrdem_arv_red_black(arvores_red_black *raiz, int H) {
    if(raiz == NULL)
        return;

    if(*raiz != NULL) {
        if((*raiz)->cor == Red)
            printf("%d Vermelho = H(%d) \n", (*raiz)->info, H);
        else
            printf("%d Preto = H(%d) \n", (*raiz)->info, H);

        preOrdem_arv_red_black(&((*raiz)->esq), H + 1);
        preOrdem_arv_red_black(&((*raiz)->dir), H + 1);
    }
}
