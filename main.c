#include <stdio.h>
#include <stdlib.h>

#include "arvores_red_black.c"

int main() {
    arvores_red_black * raiz = cria_arv_red_black();

    int i, N = 9, val[9] = {11, 2, 1, 5, 4, 7, 8, 14, 15};

    for(i = 0; i < N; i++) {
        insere_arv_red_black(raiz, val[i]);
    }

    emOrdem_arv_red_black(raiz, 0);

    printf("\n\n==========================\n\n");

    remove_arv_red_black(raiz, 4);
    emOrdem_arv_red_black(raiz, 0);

    printf("\n\n==========================\n\n");

    remove_arv_red_black(raiz, 1);
    emOrdem_arv_red_black(raiz, 0);

    return 0;
}
